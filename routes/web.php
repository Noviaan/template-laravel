<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/iniaslinya', function () {
    return view('welcome');
});


//--------------Intro Laravel----------//

Route::get('/', function () {
    return view('home');
});
Route::get('/form', 'RegisterController@form');
route::get('/welcome1', 'RegisterController@welcome1');
Route::post('/welcome1', 'RegisterController@welcome1_post');


Route::get('/sapa', 'RegisterController@sapa');
Route::post('/sapa', 'RegisterController@sapa_post');


//Memasangkan Template dengan Laravel Blade//
Route::get('/master', function(){
    return view('master');
});

route::get('/table', function(){
    return view('tabelfor.table');
});
route::get('/datatable', function(){
    return view('tabelfor.datatable');
});
